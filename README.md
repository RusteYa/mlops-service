# MLOps конвейер модели машинного обучения 

Здесь представлен подход к построению конвейера MLOps на основе инструментов DevOps и библиотек с открытым исходным кодом.
Для использования потребуется предварительно развернуть кластер Kubernetes.

Основные шаги для предварительной настройки:

1. Развернуть кластер kubernetes
2. Установить приложения Gitlab Runner на сервере для развертывания модели и высокопроизводительном сервере для выполнения остальных заданий конвейера с тегами deploy и cloud соответственно.
3. Заполнить переменные в настройках Gitlab CI/CD:
   1. GDRIVE_CREDENTIALS_DATA - данные из json файла авторизации Google Drive
   2. ML_API_REPO_URL - URL репозитория gitlab с моделью
   3. REPO_TOKEN - токен доступа к этому репозиторию
4. Развернуть сервисы Prometheus и Grafana для мониторинга в кластере kubernetes в пространстве имен monitoring с помощью пакетного менеджера Helm.
    ```
    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    helm repo update
    helm install prometheus prometheus-community/kube-prometheus-stack --namespace=monitoring --create-namespace --wait
    ```
5. Создать сервисы NodePort для доступа к сервисам Prometheus и Grafana из кластера в локальную сеть
   ```
   kubectl expose deployment prometheus-grafana -n monitoring --target-port=3000 --name=prometheus-grafana-np --type=NodePort
   kubectl expose service prometheus-operated -n monitoring --target-port=9090 --name=prometheus-operated-np --type=NodePort
   ```
6. Настроить обратный прокси в сервисе Nginx для созданных сервисов NodePort инструментов Prometheus и Grafana и микросервиса с моделью для предоставления доступа во внешнюю сеть.
7. *Для использования конвейера с экспериментами над другими моделями могут потребоваться дополнительные правки в файлах ml/ со стадиями конвейера данных и в эндпоинте /predict файла ml_api/view/http/routers/ml.py с соответствующим юзкейсом под входные данные иного формата, отличного от файла и шагов по его предобработки.*
