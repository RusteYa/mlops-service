# .gitlab-ci.yml

variables:
  PIP_CACHE_DIR: $CI_PROJECT_DIR/.cache/pip

workflow:
  rules:
    - if: $CI_COMMIT_TITLE == "commit changes of dvc.lock"
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: never
    - when: always

stages:
  - linting
  - data-model-pipeline
  - build
  - deploy

.dependencies:
  before_script:
    - pip install dvc[gdrive]==2.10.2
    - pip install poetry
    - poetry config virtualenvs.in-project true
    - poetry install --no-root

linting:
  extends: .dependencies
  image: iterativeai/cml:0-dvc2-base1-gpu
  stage: linting
  cache:
    key:
      files:
        - poetry.lock
    paths:
      - $PIP_CACHE_DIR
      - .venv
  tags:
    - cloud
  script:
    - poetry run pre-commit run -a

data-model-pipeline:
  extends: .dependencies
  image: iterativeai/cml:0-dvc2-base1-gpu
  stage: data-model-pipeline
  tags:
    - cloud
  artifacts:
    paths:
      - model/model.h5
      - metrics/
      - prepared_data/
      - dvc.lock
    reports:
      dotenv: data_model_pipeline.env
    expire_in: 1 week
  cache:
    - key:
       files:
        - poetry.lock
      paths:
      - $PIP_CACHE_DIR
      - .venv
    - key: dvc-cache
      paths:
      - .dvc
  script:
    - nvidia-smi
    - dvc pull
    - dvc status
    - STATUS=`dvc repro`
    - >
      if [[ $STATUS =~ "send your updates to remote storage" ]];
        then
          echo "STATUS_PUSH=true" >> data_model_pipeline.env
        else
          echo "STATUS_PUSH=false" >> data_model_pipeline.env
      fi
    - cat data_model_pipeline.env

    - echo '**Test data metrics**  ' >> report.md
    - dvc metrics diff --show-md main >> report.md

    - echo '**Train and validation data plots**  ' >> report.md
    - dvc plots diff --target metrics/loss.csv --show-vega main > vega.json
    - vl2png vega.json > metrics/loss.png
    - cml publish metrics/loss.png --md --title 'losses' >> report.md

    - cml publish metrics/Accuracy.png --md --title 'Accuracy' >> report.md
    - cml publish metrics/Precision.png --md --title 'Precision' >> report.md
    - cml publish metrics/Recall.png --md --title 'Recall' >> report.md
    - cml publish metrics/LossVal_loss.png --md --title 'LossVal_loss' >> report.md

    - cml send-comment report.md
    - dvc status

push-model:
  image: iterativeai/cml:latest
  stage: build
  tags:
    - cloud
  cache:
    key: dvc-cache
    paths:
      - .dvc
  needs:
    - job: data-model-pipeline
      artifacts: true
  only:
    refs:
      - main
  before_script:
    - pip install dvc[gdrive]==2.10.2
  script:
    - dvc status
    - git status
    - git tag -n
    - >
      if [[ $STATUS_PUSH == true ]]; then
        git remote set-url origin https://${GITLAB_USER_NAME}:${REPO_TOKEN}@${CI_REPOSITORY_URL#*@}
        git config user.email "bot@example.com"
        git config user.name "[Bot]"
        git add dvc.lock
        git commit -m 'commit changes of dvc.lock'
        git tag $CI_COMMIT_SHORT_SHA
        git tag -n
        git push --tags origin HEAD:$CI_COMMIT_REF_NAME
        dvc push
      fi

build-docker-images:
  image: docker:20
  stage: build
  tags:
    - cloud
  only:
    refs:
      - main
  services:
    - name: docker:dind
      alias: docker
      command: ["--tls=false"]
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
  before_script:
    - docker info
    - echo -n $CI_REGISTRY_PASSWORD | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
  script:
    - >
      docker build
      --pull
      --cache-from $CI_REGISTRY_IMAGE:latest
      --label "org.opencontainers.image.title=$CI_PROJECT_TITLE"
      --label "org.opencontainers.image.url=$CI_PROJECT_URL"
      --label "org.opencontainers.image.created=$CI_JOB_STARTED_AT"
      --label "org.opencontainers.image.revision=$CI_COMMIT_SHORT_SHA"
      --label "org.opencontainers.image.version=$CI_COMMIT_REF_NAME"
      --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
      .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA

deploy:
  stage: deploy
  needs: [build-docker-images]
  tags:
    - deploy
  only:
    refs:
      - main
  variables:
    k8s_namespace: dev-ml-api
    app_label: ml-model
  environment:
    name: dev
  script:
    - export IMAGE=$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
    - envsubst < ./.kube/deployment.templ.yaml > ./.kube/deployment.yaml
    - kubectl create namespace $k8s_namespace --dry-run=client -o yaml | kubectl apply -f -
    - kubectl apply -f ./.kube/deployment.yaml --wait=true
    - sleep 60
    - >
      while [[ $(kubectl get pods -l app=$app_label -n $k8s_namespace -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do
        echo "waiting for pod" && sleep 10;
      done