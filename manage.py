import time

import uvicorn

from ml_api.app.di import init_container


def run_http_server(host: str = "localhost", port: int = 8080) -> None:
    time.sleep(2)
    uvicorn.run("ml_api.app.http_app:app", host=host, port=port)


if __name__ == "__main__":
    container = init_container()
    run_http_server()
