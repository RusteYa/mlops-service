from typing import Any, List

from pydantic import BaseModel


class PredictRequestData(BaseModel):
    names: List[str]
    ndarray: List[List[Any]]


class PredictRequest(BaseModel):
    data: PredictRequestData
