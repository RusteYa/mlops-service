from typing import Union, Dict

from pydantic import BaseModel


class ExperimentMeta(BaseModel):
    experiment_tag: str
    date: str
    params: Dict[str, Union[float, str]]
    metrics: Dict[str, float]
