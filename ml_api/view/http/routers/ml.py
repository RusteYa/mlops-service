from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, UploadFile, File, Depends, Body, Response

from typing import List

from ml_api.app.di import AppContainer
from ml_api.use_cases.protocols import (
    GetExperimentsMetasUseCaseProto,
    ReplaceMLModelUseCaseProto,
    PredictUseCaseProto,
)
from ml_api.view.http.view_models.get_experiments_metas import ExperimentMeta

ml_management_router = APIRouter(tags=["ml_api"])


@ml_management_router.post("/predict")
@inject
async def predict(
    response: Response,
    request_file: UploadFile = File(...),
    predict_use_case: PredictUseCaseProto = Depends(
        Provide[AppContainer.predict_use_case]
    ),
) -> int:
    """Прогнозирует результат алгоритма"""

    prediction = await predict_use_case.predict(request_file)

    response.headers["X-model-score"] = prediction
    return int(prediction)


@ml_management_router.get(
    "/get_experiments_metas",
    response_model=List[ExperimentMeta],
)
@inject
async def get_experiments_metas(
    get_experiments_metas_use_case: GetExperimentsMetasUseCaseProto = Depends(
        Provide[AppContainer.get_experiments_metas_use_case]
    ),
) -> List[ExperimentMeta]:
    """Возвращает список метаданных экспериментов"""

    result = await get_experiments_metas_use_case.get_experiments_metas()
    return result


@ml_management_router.post("/replace_ml_model")
@inject
async def replace_ml_model(
    model_experiment_tag: str = Body(...),
    replace_ml_model_use_case: ReplaceMLModelUseCaseProto = Depends(
        Provide[AppContainer.replace_ml_model_use_case]
    ),
) -> None:
    """Заменяет модель машинного обучения на требуемую из реестра"""

    await replace_ml_model_use_case.replace_ml_model(model_experiment_tag)
