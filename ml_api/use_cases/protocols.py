from abc import abstractmethod
from typing import Protocol, List

from fastapi import UploadFile

from ml_api.view.http.view_models.get_experiments_metas import ExperimentMeta


class GetExperimentsMetasUseCaseProto(Protocol):
    """Протокол юз кейса вывода метаданных экспериментов"""

    @abstractmethod
    async def get_experiments_metas(self) -> List[ExperimentMeta]:
        """Возвращает список метаданных экспериментов"""


class ReplaceMLModelUseCaseProto(Protocol):
    """
    Протокол юз кейса замены используемой модели машинного обучения
    на требуемую из реестра моделей
    """

    @abstractmethod
    async def replace_ml_model(self, model_experiment_tag: str) -> None:
        """Заменяет модель машинного обучения на требуемую из реестра"""


class PredictUseCaseProto(Protocol):
    """
    Юз кейс классифицирования
    """

    @abstractmethod
    async def predict(self, request_file: UploadFile) -> str:
        """Классифицирует входные данные"""
