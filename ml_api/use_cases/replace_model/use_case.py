import subprocess
from dataclasses import dataclass

from dependency_injector import containers

from ml_api.use_cases.protocols import ReplaceMLModelUseCaseProto


@dataclass
class ReplaceMLModelUseCase(ReplaceMLModelUseCaseProto):
    """
    Юз кейс замены используемой модели машинного обучения
    на требуемую из реестра моделей
    """

    container: containers.DeclarativeContainer
    ml_repo_url: str
    model_path: str

    async def replace_ml_model(self, model_experiment_tag: str) -> None:
        """Заменяет модель машинного обучения на требуемую из реестра"""

        command = (
            f"dvc get --rev {model_experiment_tag} {self.ml_repo_url} {self.model_path}"
        )
        stdout = subprocess.run(
            command.split(), check=True, capture_output=True, text=True
        ).stdout
        print(stdout)

        self.container.model.reset()  # type: ignore
