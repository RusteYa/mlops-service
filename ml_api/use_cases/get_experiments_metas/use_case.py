import json
import subprocess
from dataclasses import dataclass
from typing import List

from ml_api.use_cases.protocols import GetExperimentsMetasUseCaseProto
from ml_api.view.http.view_models.get_experiments_metas import ExperimentMeta


@dataclass
class GetExperimentsMetasUseCase(GetExperimentsMetasUseCaseProto):
    """Юз кейс вывода метаданных экспериментов"""

    async def get_experiments_metas(self) -> List[ExperimentMeta]:
        """Возвращает список метаданных экспериментов"""

        command = "dvc exp show -T --json"
        stdout = subprocess.run(
            command.split(), check=True, capture_output=True, text=True
        ).stdout
        s = stdout.rsplit("\n")[0]
        exps = json.loads(s)

        exp_metas = []
        for name, data in exps.items():
            if name != "workspace":
                exp_data = data["baseline"]["data"]
                exp = ExperimentMeta(
                    experiment_tag=exp_data["name"],
                    date=exp_data["timestamp"],
                    params=next(iter(exp_data["params"].values()))["data"]["model"],
                    metrics=next(iter(exp_data["metrics"].values()))["data"],
                )
                exp_metas.append(exp)

        return exp_metas
