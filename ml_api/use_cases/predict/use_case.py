from dataclasses import dataclass

import numpy as np
import numpy.typing as npt
from _dlib_pybind11 import fhog_object_detector
from fastapi import UploadFile
from keras import Model

from typing import Tuple

import cv2

from ml_api.use_cases.protocols import PredictUseCaseProto


@dataclass
class PredictUseCase(PredictUseCaseProto):
    """
    Юз кейс классифицирования
    """

    model: Model
    detector: fhog_object_detector

    async def predict(self, request_file: UploadFile) -> str:
        """Классифицирует входные данные"""

        f = await request_file.read()
        image: npt.NDArray = np.asarray(bytearray(f))  # type: ignore

        face, tagged_photo = self._preprocess_data(image, 250)

        prediction = self.model.predict(face, batch_size=4)
        prediction = np.argmax(prediction, axis=1)

        return str(prediction[0])

    def _preprocess_data(
        self, img: npt.NDArray, out_resolution: int
    ) -> Tuple[npt.NDArray, npt.NDArray]:
        image = cv2.imdecode(img, cv2.IMREAD_COLOR)
        faces, tagged_photo = self._detect_face(image, out_resolution)
        faces = faces / 255
        return faces, tagged_photo

    def _detect_face(
        self, image: npt.NDArray, out_resolution: int
    ) -> Tuple[npt.NDArray, npt.NDArray]:
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        faces = self.detector(gray)
        if len(faces) > 0:
            tl_x = faces[0].tl_corner().x
            tl_y = faces[0].tl_corner().y
            tl_h = faces[0].height()
            tl_w = faces[0].width()
            if tl_x > 0 and tl_y > 0 and tl_h > 10 and tl_w > 10:
                image = image[tl_y : tl_y + tl_w, tl_x : tl_x + tl_h, :]
        im = cv2.resize(
            image, (out_resolution, out_resolution), interpolation=cv2.INTER_LINEAR
        )
        return np.array([im]), image
