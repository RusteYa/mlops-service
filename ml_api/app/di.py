import dlib
from dependency_injector import containers, providers
from keras.models import load_model, Model

from ml_api.app.config import MLConfig
from ml_api.use_cases.get_experiments_metas.use_case import GetExperimentsMetasUseCase
from ml_api.use_cases.predict.use_case import PredictUseCase
from ml_api.use_cases.replace_model.use_case import ReplaceMLModelUseCase


class AppContainer(containers.DeclarativeContainer):
    """IoC-контейнер с зависимостями"""

    __self__ = providers.Self()  # type: ignore

    config: MLConfig = providers.Configuration()  # type: ignore

    model: Model = providers.Singleton(load_model, filepath=config.model_path)

    get_experiments_metas_use_case = providers.Factory(GetExperimentsMetasUseCase)
    replace_ml_model_use_case = providers.Factory(
        ReplaceMLModelUseCase,
        container=__self__,
        ml_repo_url=config.repo_url,
        model_path=config.model_path,
    )
    predict_use_case = providers.Factory(
        PredictUseCase,
        model,
        providers.Resource(dlib.get_frontal_face_detector),
    )


def init_container() -> AppContainer:
    """Создает контейнер"""

    container = AppContainer()
    container.config.from_pydantic(MLConfig())  # type: ignore

    return container
