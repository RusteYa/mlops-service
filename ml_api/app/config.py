from pydantic import BaseSettings


class MLConfig(BaseSettings):
    """Настройки приложения"""

    repo_url: str
    model_path: str = "./model/model.h5"

    class Config:
        env_prefix = "ML_API_"
        env_file = ".env"
        env_file_encoding = "utf-8"
