from typing import Any, List

from fastapi import FastAPI

import ml_api
from ml_api.app.di import init_container
from ml_api.app.monitoring import instrumentator
from ml_api.view.http.routers.ml import ml_management_router


def create_app() -> FastAPI:
    """Создает приложения, предварительно создав контейнер"""

    container = init_container()
    container.wire(packages=[ml_api])

    global_dependencies: List[Any] = []
    app_ = FastAPI(dependencies=global_dependencies)

    app_.include_router(ml_management_router)

    return app_


app = create_app()

instrumentator.instrument(app).expose(app, include_in_schema=False, should_gzip=True)
