import os
from enum import Enum
from typing import Tuple

import numpy as np
import cv2
import numpy.typing as npt
from sklearn.preprocessing import OneHotEncoder
import dlib


class BCOLORS(str, Enum):
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


detector = dlib.get_frontal_face_detector()
face_cascade = cv2.CascadeClassifier(
    cv2.data.haarcascades + "haarcascade_frontalface_default.xml"
)
eyes_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_eye.xml")


def detect_face(
    image: npt.NDArray, out_resolution: int
) -> Tuple[npt.NDArray, npt.NDArray]:
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    faces = detector(gray)
    if len(faces) > 0:
        tl_x = faces[0].tl_corner().x
        tl_y = faces[0].tl_corner().y
        tl_h = faces[0].height()
        tl_w = faces[0].width()
        if tl_x > 0 and tl_y > 0 and tl_h > 10 and tl_w > 10:
            image = image[tl_y : tl_y + tl_w, tl_x : tl_x + tl_h, :]
    im = cv2.resize(
        image, (out_resolution, out_resolution), interpolation=cv2.INTER_LINEAR
    )
    return np.array([im]), image


def image_preparation(img_path: str, new_img_path: str) -> bool:
    image = cv2.imread(img_path)
    faces, tagged_photo = detect_face(image, 250)
    if len(faces) != 0:
        os.makedirs(os.path.dirname(new_img_path), exist_ok=True)
        cv2.imwrite(new_img_path, faces[0])
        return True
    else:
        print(f"{BCOLORS.FAIL} Face not found " + img_path + f"{BCOLORS.ENDC}")
        return False


def setup(base: str, prepare_image_base: str) -> Tuple[npt.NDArray, npt.NDArray]:
    paths = []
    labels = []
    states = ["alert", "non_vigilant", "tired"]
    for label in states:
        temp_base = base + f"/{label}"
        prepare_image_temp_base = prepare_image_base + f"/{label}"
        for img in os.listdir(temp_base):
            if image_preparation(
                temp_base + f"/{img}", prepare_image_temp_base + f"/{img}"
            ):
                paths.append(prepare_image_temp_base + f"/{img}")
                labels.append(label)

    enc = OneHotEncoder(sparse=False)
    labels_ = np.reshape(labels, (-1, 1))
    enc.fit(labels_)
    labels_ = enc.transform(labels_)
    return np.array(paths), labels_


train_paths, train_labels = setup("./data/train", "./prepared_data/train")
test_paths, test_labels = setup("./data/test", "./prepared_data/test")
val_paths, val_labels = setup("./data/val", "./prepared_data/val")
