import os
from typing import Tuple

import numpy as np
import numpy.typing as npt
import cv2
import pandas as pd
import json
import joblib
import matplotlib.pyplot as plt
import seaborn as sns
from keras.models import load_model
from matplotlib.ticker import MaxNLocator
from sklearn.metrics import accuracy_score, recall_score, precision_score
from sklearn.preprocessing import OneHotEncoder


def setup_data(base: str, prepare_image_base: str) -> Tuple[npt.NDArray, npt.NDArray]:
    paths = []
    labels = []
    states = ["alert", "non_vigilant", "tired"]
    for label in states:
        temp_base = base + f"/{label}"
        prepare_image_temp_base = prepare_image_base + f"/{label}"
        for img in os.listdir(temp_base):
            paths.append(prepare_image_temp_base + f"/{img}")
            labels.append(label)

    enc = OneHotEncoder(sparse=False)
    labels_ = np.reshape(labels, (-1, 1))
    enc.fit(labels_)
    labels_ = enc.transform(labels_)
    return np.array(paths), labels_


def evaluate_model() -> None:
    model = load_model("./model/model.h5")

    test_paths, test_labels = setup_data("./data/test", "./prepared_data/test")

    labels = np.argmax(test_labels, axis=1)
    X = np.zeros((test_paths.shape[0], 250, 250, 3))
    for i, path in enumerate(test_paths):
        X[i] = cv2.imread(path) / 255
    predict = model.predict(X)
    predict_ = np.argmax(predict, axis=1)

    acc = accuracy_score(labels, predict_)
    prec = precision_score(labels, predict_, average="macro")
    rec = recall_score(labels, predict_, average="macro")

    with open("./metrics/metrics.json", "w") as outfile:
        json.dump({"accuracy": acc, "precision": prec, "recall": rec}, outfile)


def make_plots() -> None:
    model_history = joblib.load("./metrics/model_history.pkl")

    plt.style.use("seaborn")
    plt.rcParams["figure.figsize"] = (16, 10)
    sns.set(style="whitegrid", font_scale=2)

    loss = model_history["val_loss"]
    pd.DataFrame(loss, columns=["loss"]).to_csv("./metrics/loss.csv", index=False)

    fig, ax = plt.subplots()
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.set_title(label="Losses", size=36, weight="bold")
    ax.set_xlabel("Epoch")
    ax.set_ylabel("Value")

    ax.plot(model_history["loss"], label="train loss")
    ax.plot(model_history["val_loss"], label="validation loss")

    plt.legend()
    plt.savefig("./metrics/LossVal_loss")

    fig, ax = plt.subplots()
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.set_title(label="Accuracy", size=36, weight="bold")
    ax.set_xlabel("Epoch")
    ax.set_ylabel("Value")

    ax.plot(model_history["acc"], label="train accuracy")
    ax.plot(model_history["val_acc"], label="validation accuracy")

    plt.legend()
    plt.savefig("./metrics/Accuracy")

    fig, ax = plt.subplots()
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.set_title(label="Precision", size=36, weight="bold")
    ax.set_xlabel("Epoch")
    ax.set_ylabel("Value")

    ax.plot(model_history["precision"], label="train precision")
    ax.plot(model_history["val_precision"], label="validation precision")

    plt.legend()
    plt.savefig("./metrics/Precision")

    fig, ax = plt.subplots()
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.plot(model_history["recall"], label="train recall")
    ax.plot(model_history["val_recall"], label="validation recall")

    plt.legend()
    plt.savefig("./metrics/Recall")


evaluate_model()
make_plots()
