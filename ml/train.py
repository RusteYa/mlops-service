import os
from typing import Tuple, Iterable

import tensorflow as tf
import numpy as np
import numpy.typing as npt

import joblib
from keras.applications.resnet_v2 import ResNet152V2
from keras.layers import Dense
from keras.models import Sequential
import cv2
from keras.utils.data_utils import Sequence
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
from keras.optimizers import adam_v2

from sklearn.preprocessing import OneHotEncoder
import yaml


def setup_data(base: str, prepare_image_base: str) -> Tuple[npt.NDArray, npt.NDArray]:
    paths = []
    labels = []
    states = ["alert", "non_vigilant", "tired"]
    for label in states:
        temp_base = base + f"/{label}"
        prepare_image_temp_base = prepare_image_base + f"/{label}"
        for img in os.listdir(temp_base):
            paths.append(prepare_image_temp_base + f"/{img}")
            labels.append(label)

    enc = OneHotEncoder(sparse=False)
    labels_ = np.reshape(labels, (-1, 1))
    enc.fit(labels_)
    labels_ = enc.transform(labels_)
    return np.array(paths), labels_


print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices("GPU")))

params = yaml.safe_load(open("./params.yaml"))["model"]
epochs = params["epochs"]
loss = params["loss"]
learning_rate = params["learning_rate"]

train_paths, train_labels = setup_data("./data/train", "./prepared_data/train")
val_paths, val_labels = setup_data("./data/val", "./prepared_data/val")


class FatigueSequence(Sequence):
    def __init__(
        self,
        paths: npt.NDArray,
        labels: npt.NDArray,
        batch_size: int,
        shape: Tuple[int, int, int],
        shuffle: bool = True,
    ) -> None:
        self.paths, self.labels = paths, labels
        self.batch_size = batch_size
        self.shape = shape
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self) -> int:
        return int(np.ceil(len(self.paths) / float(self.batch_size)))

    def __getitem__(self, idx: int) -> Tuple[npt.NDArray, npt.NDArray]:
        indexes = self.indexes[idx * self.batch_size : (idx + 1) * self.batch_size]

        paths = self.paths[indexes]
        X = np.zeros((paths.shape[0], self.shape[0], self.shape[1], self.shape[2]))
        for i, path in enumerate(paths):
            X[i] = self.load_image(path)

        y = self.labels[indexes]
        return X, y

    def on_epoch_end(self) -> None:
        self.indexes = np.arange(len(self.paths))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def __iter__(self) -> Iterable:
        for item in (self[i] for i in range(len(self))):
            yield item

    def load_image(self, path: npt.NDArray) -> npt.NDArray:
        image = cv2.imread(path)
        return image / 255


train_datagen = FatigueSequence(
    train_paths, train_labels, batch_size=32, shape=(250, 250, 3), shuffle=True
)
val_datagen = FatigueSequence(
    val_paths, val_labels, batch_size=32, shape=(250, 250, 3), shuffle=True
)

tf.keras.backend.clear_session()

model = Sequential()

res_net = ResNet152V2(
    include_top=False,
    pooling="avg",
    weights="imagenet",
)
model.add(res_net)
model.add(Dense(3, activation="softmax"))

model.layers[0].trainable = False

opt = adam_v2.Adam(learning_rate=learning_rate)
estop = EarlyStopping(
    monitor="val_loss", min_delta=0, patience=10, mode="min", verbose=2
)
reduce_lr_loss = ReduceLROnPlateau(
    monitor="val_loss",
    factor=0.1,
    patience=5,
    min_delta=0.0001,
    min_lr=0.01,
    mode="min",
    verbose=2,
)
metrics = [
    "acc",
    tf.keras.metrics.AUC(),
    tf.keras.metrics.Precision(),
    tf.keras.metrics.Recall(),
    tf.keras.metrics.TruePositives(),
    tf.keras.metrics.TrueNegatives(),
    tf.keras.metrics.FalsePositives(),
    tf.keras.metrics.FalseNegatives(),
    tf.keras.metrics.PrecisionAtRecall(recall=0.8),
]
model.compile(optimizer=opt, loss=loss, metrics=metrics)
model.fit(
    train_datagen,
    callbacks=[estop, reduce_lr_loss],
    batch_size=train_datagen.batch_size,
    steps_per_epoch=len(train_datagen),
    validation_data=val_datagen,
    validation_batch_size=val_datagen.batch_size,
    validation_steps=len(val_datagen),
    epochs=epochs,
    workers=16,
    verbose=1,
)

joblib.dump(model.history.history, "./metrics/model_history.pkl")
model.save("./model/model.h5")
