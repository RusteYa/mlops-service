FROM python:3.8 as base
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | POETRY_HOME=/opt/poetry python && \
    cd /usr/local/bin && \
    ln -s /opt/poetry/bin/poetry && \
    poetry config virtualenvs.create false

FROM base AS deps
WORKDIR /app
COPY pyproject.toml poetry.lock /app/
RUN poetry install --no-root --no-dev

FROM deps AS main
COPY . /app
RUN for remote in `git branch -r`; do git branch --track ${remote#origin/} $remote; done
RUN git ls-remote --tags origin
RUN git fetch --tags
RUN git tag -n